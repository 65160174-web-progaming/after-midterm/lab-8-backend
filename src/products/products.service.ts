import { Injectable } from '@nestjs/common';
import { CreateProductDto } from './dto/create-product.dto';
import { UpdateProductDto } from './dto/update-product.dto';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { Product } from './entities/product.entity';

@Injectable()
export class ProductsService {
  constructor(@InjectRepository(Product) private productsRepository: Repository<Product>) {}
  create(createProductDto: CreateProductDto) {
    return this.productsRepository.save(createProductDto);
  }

  findAll() {
    return this.productsRepository.find({relations: ['type']});
  }

  findOne(id: number) {
    return this.productsRepository.findOneOrFail({where: {id}, relations: ['type']});
  }

  async update(id: number, updateProductDto: UpdateProductDto) {
    const updateProduct = await this.productsRepository.findOneByOrFail({id});
    await this.productsRepository.update(id,{ ...updateProduct,...updateProductDto});
    const updatedProduct = await this.productsRepository.findOneBy({id});


    return updatedProduct;
  }

  async remove(id: number) {
    const deletedProduct = await this.productsRepository.findOneOrFail({where: {id},relations: ['type']});
    await this.productsRepository.remove(deletedProduct);
    return deletedProduct;
  }
}
