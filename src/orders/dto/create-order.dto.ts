export class CreateOrderDto {
    userId: number;
    orderItems: {
        productId: number;
        qty: number;
    }[]
}
